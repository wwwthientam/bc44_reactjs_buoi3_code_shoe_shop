import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    let { cart, handleDelete, handleChangeAmount } = this.props;
    return (
      <div className="col-6">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Stock</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td>{item.quantity}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleChangeAmount(item.id, -1);
                      }}
                      className="btn btn-success"
                    >
                      -
                    </button>
                    <strong className="mx-3">{item.soLuong}</strong>
                    <button
                      onClick={() => {
                        handleChangeAmount(item.id, +1);
                      }}
                      className="btn btn-success"
                    >
                      +
                    </button>
                  </td>
                  <td>{item.price * item.soLuong}</td>
                  <td>
                    <img style={{ width: 50 }} src={item.image} alt="Picture" />
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        handleDelete(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
