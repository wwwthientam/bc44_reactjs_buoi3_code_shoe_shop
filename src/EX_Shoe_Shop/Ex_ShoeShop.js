import React, { Component } from "react";
import { shoeArr } from "./Data";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleChangeDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == shoe.id);
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (idshoe) => {
    let cloneCart = this.state.cart.filter((item) => item.id !== idshoe);
    this.setState({
      cart: cloneCart,
    });
  };
  handleChangeAmount = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == idShoe);
    cloneCart[index].soLuong = cloneCart[index].soLuong + option;
    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <h1 className="text-center text-success">Ex Shoe Shop</h1>
        <DetailShoe detail={this.state.detailShoe} />
        <div className="row">
          <Cart
            handleDelete={this.handleDelete}
            handleChangeAmount={this.handleChangeAmount}
            cart={this.state.cart}
          />
          <ListShoe
            addToCart={this.handleAddToCart}
            viewDetail={this.handleChangeDetail}
            list={this.state.shoeArr}
          />
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}
