import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, watchDetail, addToCart } = this.props;
    let { image, name } = data;
    return (
      <div className="col-6 p-4">
        <div class="card text-left h-100 border-danger">
          <img class="card-img-top" src={image} alt="Picture" />
          <div class="card-body">
            <h4 class="card-title">{name}</h4>
          </div>
          <button
            onClick={() => {
              watchDetail(data);
            }}
            className="btn btn-danger"
          >
            Xem
          </button>
          <button
            onClick={() => {
              addToCart(data);
            }}
            className="btn btn-primary"
          >
            Mua
          </button>
        </div>
      </div>
    );
  }
}
