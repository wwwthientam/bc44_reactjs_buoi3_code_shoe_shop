import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShop from "./EX_Shoe_Shop/Ex_ShoeShop";

function App() {
  return <Ex_ShoeShop />;
}

export default App;
